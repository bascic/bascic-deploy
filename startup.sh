#!/bin/bash

while getopts ":p:" opt; do
  case $opt in
    p)
      echo "-p enabled production deployment, Parameter: $OPTARG" >&2
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

export HOME=$(pwd)
export SAMHSA_REPO="https://bitbucket.org/lu_anna/samhsa-dev"
export DEXINLP_REPO="https://bitbucket.org/lu_anna/dexinlp"

# system
sudo apt-get -qq update

# firewall networking
sudo ufw -f enable
sudo ufw reload

# virutal environment
sudo apt-get install python3-virtualenv
virtualenv --python=python3 venv
source venv/bin/activate

# docker
wget https://download.docker.com/linux/ubuntu/dists/trusty/pool/stable/amd64/docker-ce_17.03.0~ce-0~ubuntu-trusty_amd64.deb
sudo dpkg -i docker-ce_17.03.0~ce-0~ubuntu-trusty_amd64.deb
sudo apt-get -qq -f install
sudo apt-get -qq -y install docker-ce
rm docker-ce_17.03.0~ce-0~ubuntu-trusty_amd64.deb

# docker-compose
sudo curl -L https://github.com/docker/compose/releases/download/1.17.0/docker-compose-	`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# dexinlp and bascic
cd config
sudo docker build -t bascic_backend .
sudo docker run -p 80:5000 -d $(docker images -q bascic_backend)

# orientdb
tar -xvf orientdb.tar.gz
cd orientdb
sudo docker-compose up --build

# docker container networking
sudo docker network create app_db_network
sudo docker network connect app_db_network sphinx-server

# documentation
pip install sphinx
pip install sphinx_rtd_theme
sudo docker pull dldl/sphinx-server
sudo docker run -d -v "$(pwd)":/web -u $(id -u):$(id -g) -p 35729:35729 -p 8000:8000 --name sphinx-server dldl/sphinx-server
sudo docker exec -dt sphinx-server 'config/gendocs.sh'

# status
sudo docker ps -a
